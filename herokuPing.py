from platform   import system as system_name  # Returns the system/OS name
from subprocess import call   as system_call  # Execute a shell command
# Use Cron Job 0 8-18/2 * * * /path/command
def ping(host):
    """
    Returns True if host (str) responds to a ping request.
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Ping command count option as function of OS
    param = '-n' if system_name().lower()=='windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    # Pinging
    return system_call(command) == 0

class siteItem:
  """
  Sites to check containing url, name and description
  """
  def __init__(self, url, urlName, description,status=None):
    self.url = url
    self.urlName = urlName
    self.description = description
    if status is None:
        self.status = 'ok'
test = ping("google.com")
print(test)
exit
from jinja2 import Template
jinja_template = "./templatePing.html"

with open(jinja_template) as f:
    tmpl = Template(f.read())

# URL, Name of Site, Description, status is appended
# These fields match the templatePing.html
# Note to self can define class of "sites" instead of hardcoded array

# Add syom website, etc ... 
# My mortgage aunction site 
siteInfo = [
    siteItem('dli-discord-rss.herokuapp.com','Discord Rss','Personal Heroku App'),
    siteItem('todoListDapp.surge.sh','TodoList Dapp','TOdo List DApp, pretty basic'),
    siteItem('harvestmarket.xyz','Harvest','Tokenzied Application with ERC20 and ERC721 token standards, I built most of this DApp'),
    siteItem('treblekey.com','Treblekey','Treblekey -- Blockchain Site'),
    siteItem('minermonitor.xyz','Miner Monitor','Miner Monitor is a site'),
    siteItem('membranlabs.com','Membran Labs','Worked on Harvest Theme'),
    siteItem('friendlyuser.github.io','FriendlyUser','Github Account'),
    siteItem('david-li.me','Custom Site','Github Account'),
    siteItem('tex-diagrams.david-li.me','Tex','Github Account')
]
print("this will fail")
for i in range(len(siteInfo)):
    site = siteInfo[i]
    #print(site)
    url = site.url
    status = ping(url)
    print(status)
    if status: 
        siteInfo[i].status = 'ok'
    else:
        siteInfo[i].status = 'disrupted'

jinja_output =tmpl.render(
    siteList = siteInfo
)
f.close()

with open('index.html','w',encoding='utf-8') as f:
    f.write(jinja_output)

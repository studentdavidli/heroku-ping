# heroku-ping


Sends a ping message to my various heroku repos and surge sites including 

https://test-testtestestest.herokuapp.com/

http://todoListDapp.surge.sh


### Tasks

* Gitlab Pipeline to automatically ping servers
* Python script to check results of pings 
* Create Jinja template based off of cstate 
* Publish static site every 3 hours (cron job in pipeline)


#### Websites that are left out 


* https://dli-redmine.herokuapp.com/ --- will probably check this repo every day, my current strategy is to write tasks in boostnote, if they will take a while, link to a redmine issue and give more details, and perhaps link to pull requests.
#!/bin/bash

# set the url to probe, hardcoded, cause I'm lazy
url=https://dli-discord-rss.herokuapp.com/

if [ "$url" = "" ]; then
   echo "D: you did not supply a url!"
   exit
fi

# use curl to request headers (return sensitive default on timeout: "timeout 500"). Parse the result into an array (avoid settings IFS, instead use read)
read -ra result <<< $(curl -Is --connect-timeout 5 "${url}" || echo "timeout 500")
# status code is second element of array "result"
status=${result[1]}
# if status code is greater than or equal to 400, then output a bounce message (replace this with any bounce script you like)
#[ $status -ge 400  ] && echo "bounce at $url with status $status"

if [[ "$status" -ge 400 ]] ; then
  echo "bounce at $url with status $status"
  exit 10 #hopefully this causes an error in a gitlab pipeline
else
  exit 0
fi